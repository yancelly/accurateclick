##Écran d'accueil##

Titre du jeu
Les boutons :
- Bouton play   => Boutons mode de jeu
- Bouton tableau des scores
- Bouton quit


##Fenêtre de jeu##

Le joueur doit appuyer sur un point qui se déplace dès qu'on le touche.
Modes de jeu :
- L'objectif est de toucher ce point le plus de fois possible pour monter au plus haut niveau en un temps limité.
- L'objectif est de toucher ce point un nombre de fois défini et atteindre le niveau voulu, le plus rapidement possible.


##Tableau des scores##

Une présentation sous forme de liste (chapitre 6)
Une sauvegarde persistante pour au moins l'une des deux plateformes
On affichera les derniers meilleurs scores par :
- Précision
- Nombre de tap


##Responsive##

L’application doit fonctionner correctement sur tout type d’écran (grand, petit...)
En mode portrait et paysage : ressources alternatives sous Android
